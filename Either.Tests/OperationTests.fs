﻿namespace Either.Tests

open FSharp.Either
open NUnit.Framework
open FsUnit

[<TestFixture>]
type ``Operation tests``() =
    [<Test>]
    member x.``Swapping left and right with type member``() = 
        let e = Left(2)
        let result = e.Swap();
        result |> should equal (Right(2))
        result.Swap() |> should equal e

    [<Test>]
    member x.``Swapping left and right with module function``() = 
        let e = Left(2)
        let result = e |> Either.swap 
        result |> should equal (Right(2))
        result |> Either.swap |> should equal e

    [<Test>]
    member x.``Either.swapped over Right``() =
        match Right(10) |> Either.swapped (Either.map(fun a -> "2014-05-06: " + a.ToString())) with
        | Left(_) -> Assert.Fail()
        | Right(b) -> b |> should equal 10
    
    [<Test>]
    member x.``Either.swapped over Left``() =
        match Left("No value!") |> Either.swapped (Either.map(fun s -> "2014-05-06: " + s)) with
        | Left(a) -> a |> should equal "2014-05-06: No value!"
        | Right(_) -> Assert.Fail()

    [<Test>]
    member x.``Either.map over Right``() =
        let a = Some(5)
        Right(5) |> Either.map(fun i -> i * 2) |> should equal (Right(10))
    
    [<Test>]
    member x.``Either.map over Left``() =
        match Left(5) |> Either.map(fun i -> i * 2) with
        | Left(a) -> a |> should equal 5
        | Right(_) -> Assert.Fail()

    [<Test>]
    member x.``Either.isLeft should be false on Right``() =
        Right("abc") |> Either.isLeft |> should be False

    [<Test>]
    member x.``Either.isLeft should be true on Left``() =
        Left("abc") |> Either.isLeft |> should be True

    [<Test>]
    member x.``Either.isRight should be true on Right``() =
        Right("abc") |> Either.isRight |> should be True

    [<Test>]
    member x.``Either.isRight should be false on Left``() =
        Left("abc") |> Either.isRight |> should be False

    [<Test>]
    member x.``Either.fold over Right``() =
        Right(3) |> Either.fold String.length (fun i -> i + 1)  |> should equal 4

    [<Test>]
    member x.``Either.fold over Left``() =
        Left("No result!") |> Either.fold String.length (fun i -> i + 1)  |> should equal 10
    
    [<Test>]
    member x.``Either.bimap over Right``() =
        match Right(2.5) |> Either.bimap (fun s -> System.Exception(s)) ((*) 2.0) with
        | Left(a) -> Assert.Fail()
        | Right(b) -> b |> should equal 5.0

    [<Test>]
    member x.``Either.bimap over Left``() =
        match Left("Error 12314") |> Either.bimap (fun s -> System.Exception(s)) ((*) 2.0) with
        | Left(a) -> a.Message |> should equal "Error 12314"
        | Right(_) -> Assert.Fail()

    [<Test>]
    member x.``Either.leftMap over Right``() =
        match Right("abc") |> Either.leftMap String.length with
        | Left(_) -> Assert.Fail()
        | Right(b) -> b |> should equal "abc"

    [<Test>]
    member x.``Either.leftMap over Left``() =
        match Left("That's an error!") |> Either.leftMap String.length with
        | Left(a) -> a |> should equal 16
        | Right(b) -> Assert.Fail()

    [<Test>]
    member x.``Either.iter over Right``() =
        Right(6) |> Either.iter (fun i -> Assert.Pass("Executing side-effect on right with value " + i.ToString()))
        Assert.AreEqual(1, Assert.Counter)

    [<Test>]
    member x.``Either.iter over Left``() =
        let e = Left("abc")
        e |> Either.iter(fun i -> Assert.Fail("Should not execute"))
        Assert.Pass()

    [<Test>]
    member x.``Either.bind over Right``() =
        let fn a = if a % 2 = 0 then Right(a - 1) else Left("Number is not even")
        match Right(10) |> Either.bind fn with
        | Left(_) -> Assert.Fail()
        | Right(b) -> b |> should equal 9

        match Right(9) |> Either.bind fn with
        | Left(a) -> a |> should equal "Number is not even"
        | Right(_) -> Assert.Fail()
        
    [<Test>]
    member x.``Either.bind over Left``() =
        let fn a = if a % 2 = 0 then Right(a - 1) else Left("Number is not even")
        match Left("Just an error") |> Either.bind fn with
        | Left(a) -> a |> should equal "Just an error"
        | Right(_) -> Assert.Fail()
    
    [<Test>]
    member x.``>>= is an operator alias for Either.bind``() =
        let fn a = if a % 2 = 0 then Right(a - 1) else Left("Number is not even")
        match Right(10) >>= fn with
        | Left(_) -> Assert.Fail()
        | Right(b) -> b |> should equal 9

    [<Test>]
    member x.``Either.toSeq over Right returns a sequence with one element``() =
        Right(123) |> Either.toSeq |> should equal [123]

    [<Test>]
    member x.``Either.toSeq over Left returns empty sequence``() =
        Left("Testing!") |> Either.toSeq |> should be Empty

    [<Test>]
    member x.``Either.toList over Right returns list with one element``() =
        Right(123) |> Either.toList |> should equal [123]

    [<Test>]
    member x.``Either.toList over Left returns empty list``() =
        Left("Testing!") |> Either.toList |> should equal []

    [<Test>]
    member x.``Either.toArray over Right returns an array with one element``() =
        Right(123) |> Either.toArray |> should equal [|123|]

    [<Test>]
    member x.``Either.toArray over Left returns empty array``() =
        Left("Testing!") |> Either.toArray |> should be Empty

    [<Test>]
    member x.``Either.toOption over Right returns Some``() =
        Right(441) |> Either.toOption |> should equal (Some(441))

    [<Test>]
    member x.``Either.toOption over Left returns None``() =
        Left("Error 1231") |> Either.toOption |> should equal None

    [<Test>]   
    member x.``Either.getOrElse over Right returns the Right value``() =
        Right(123) |> Either.getOrElse (fun () -> 999) |> should equal 123

    [<Test>]
    member x.``Either.getOrElse over Left returns the value from function evaluation``()=
        Left("No value") |> Either.getOrElse (fun () -> 1024) |> should equal 1024

    [<Test>]
    member x.``Either.orElse over Right returns Right``() =
        match Right(554) |> Either.orElse Left with
        | Right(b) -> b |> should equal 554
        | Left(_) -> Assert.Fail()

    [<Test>]
    member x.``Either.orElse over Left runs Either from function evaluation``() =
        match Left("No value") |> Either.orElse (fun () -> Right(4)) with
        | Right(b) -> b |> should equal 4
        | Left(b) -> Assert.Fail()
            
        match Left("No value") |> Either.orElse (fun () -> Left(System.InvalidOperationException("Testing"))) with
        | Right(_) -> Assert.Fail()
        | Left(b) -> b.Message |> should equal "Testing"
            
    