﻿namespace FSharp.Either

/// <summary>Right-biased disjunction type</summary>
/// <remarks>Based on Scalaz Either: https://github.com/scalaz/scalaz/blob/scalaz-seven/core/src/main/scala/scalaz/Either.scala</remarks>
/// <typeparam name="a">Left type</typeparam>
/// <typeparam name="b">Right type</typeparam>
type Either<'a, 'b> =
    /// <summary>Constructs left disjunction value</summary>
    /// <remarks>By convention, left disjunction value should indicate an error</remarks>
    /// <typeparam name="a">Left value type</typeparam>
    | Left of 'a

    /// <summary>Constructs right disjunction value</summary>
    /// <remarks>By convention, right disjunction value should indicate success</remarks>
    /// <typeparam name="b">Right value type</typeparam>
    | Right of 'b

    /// <summary>Swaps the <see cref="Either{a,b}"></see>. <para><see cref="T:FSharp.Either.Either`2.Left"></see> becomes <see cref="T:FSharp.Either.Either`2.Right"></see>
    /// and <see cref="T:FSharp.Either.Either`2.Right"></see> becomes <see cref="T:FSharp.Either.Either`2.Left"></see> </para></summary>
    /// <returns> New, swapped <see cref="T:FSharp.Either.Either`2"></see> instance </returns>
    member Swap : unit -> Either<'b, 'a>

/// <summary>Module holding operations pertaining to <see cref="T:FSharp.Either.Either`2"></see> type. </summary>
module Either =
    /// <summary> Map on the right side of disjunction</summary>
    /// <param name="mapper">Mapping function</param>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns>New <see cref="T:FSharp.Either.Either`2"></see> instance with its <see cref="T:FSharp.Either.Either`2.Right"></see> value mapped</returns>
    val map : ('b -> 'c) -> Either<'a, 'b> -> Either<'a, 'c>

    /// <summary> Swaps the left and right values in disjunction. See also: <seealso cref="T:FSharp.Either.Either`2.Swap"></seealso></summary>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns> New <see cref="T:FSharp.Either.Either`2"></see> instance with its left and right values flipped </returns>
    val inline swap : Either<'a, 'b> -> Either<'b, 'a>

    /// <summary>Executes function <paramref name="f"></paramref> on temporarily swapped <see cref="T:FSharp.Either.Either`2"></see> instance. See also: <seealso cref="Either.swap"></seealso></summary>
    /// <param name="f">Mapping function</param>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns> New <see cref="T:FSharp.Either.Either`2"></see> instance with left and right values swapped after function execution.</returns>
    val swapped : (Either<'b, 'a> -> Either<'c, 'd>) -> Either<'a, 'b> -> Either<'d, 'c>

    /// <summary> Tests whether the disjunction is a <see cref="T:FSharp.Either.Either`2.Left"></see> value</summary>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns> <c>true</c> if the tested value is <see cref="T:FSharp.Either.Either`2.Left"></see>, <c>false</c> otherwise.</returns>
    val isLeft : Either<'a, 'b> -> bool

    /// <summary> Tests whether the disjunction is a <see cref="T:FSharp.Either.Either`2.Right"></see> value</summary>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns> <c>true</c> if the tested value is <see cref="T:FSharp.Either.Either`2.Right"></see>, <c>false</c> otherwise.</returns>
    val isRight : Either<'a, 'b> -> bool

    /// <summary> Folds the <paramref name="either"></paramref>, running the first function if <paramref name="either"></paramref> is <see cref="T:FSharp.Either.Either`2.Left"></see> or
    /// second function if <paramref name="either"></paramref> is <see cref="T:FSharp.Either.Either`2.Right"></see></summary>
    /// <param name="lfolder">Left value folder</param>
    /// <param name="rfolder">Right value folder</param>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns> Single, folded value from <paramref name="either"></paramref></returns>
    val fold : ('a -> 'c) -> ('b -> 'c) -> Either<'a, 'b> -> 'c

    /// <summary> Binary map, which maps over both <see cref="T:FSharp.Either.Either`2.Right"></see> and <see cref="T:FSharp.Either.Either`2.Left"></see> values </summary>
    /// <param name="lmap">Left value mapper</param>
    /// <param name="rmap">Right value mapper</param>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns> New <see cref="T:FSharp.Either.Either`2"></see> instance with both <see cref="T:FSharp.Either.Either`2.Right"></see> and <see cref="T:FSharp.Either.Either`2.Left"></see> values mapped </returns> 
    val bimap : ('a -> 'c) -> ('b -> 'd) -> Either<'a, 'b> -> Either<'c, 'd>

    /// <summary> Maps over the <see cref="T:FSharp.Either.Either`2.Left"></see> value of the disjunction. </summary>
    /// <param name="lmap">Left value mapper</param>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns> New <see cref="T:FSharp.Either.Either`2"></see> instance with its <see cref="T:FSharp.Either.Either`2.Left"></see> value mapped </returns>
    val leftMap : ('a -> 'c) -> Either<'a, 'b> -> Either<'c, 'b>

    /// <summary> Performs a side-effect on the <see cref="T:FSharp.Either.Either`2.Right"></see> value of the disjunction </summary>
    /// <param name="action">Action to be executed on the <see cref="T:FSharp.Either.Either`2.Right"></see> value of the disjunction </param>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    val iter : ('b -> unit) -> Either<'a, 'b> -> unit

    /// <summary>Binds the <see cref="T:FSharp.Either.Either`2.Right"></see> value of the disjunction</summary>
    /// <param name="binder">Binding function</param>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns> New <see cref="T:FSharp.Either.Either`2"></see> instance with <see cref="T:FSharp.Either.Either`2.Right"></see> value bound</returns>
    val bind : ('b -> Either<'a, 'c>) -> Either<'a, 'b> -> Either<'a,'c>

    /// <summary> Converts <paramref name="either"></paramref> into a sequence.
    /// <para> <see cref="T:FSharp.Either.Either`2.Right"></see> value is converted into a singleton sequence, while <see cref="T:FSharp.Either.Either`2.Left"></see> value is converted into an empty sequence </para> </summary>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns>A singleton sequence if <paramref name="either"></paramref> is <see cref="T:FSharp.Either.Either`2.Right"></see>, empty sequence otherwise.</returns>
    val toSeq : Either<'a, 'b> -> seq<'b>

    /// <summary> Converts <paramref name="either"></paramref> into a list. 
    /// <para> <see cref="T:FSharp.Either.Either`2.Right"></see> value is converted into a list with single element, while <see cref="T:FSharp.Either.Either`2.Left"></see> value is converted into an empty list </para> </summary>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns>A list with one element if <paramref name="either"></paramref> is <see cref="T:FSharp.Either.Either`2.Right"></see>, empty list otherwise.</returns>
    val toList : Either<'a, 'b> -> 'b list

    /// <summary> Converts <paramref name="either"></paramref> into an array. 
    /// <para> <see cref="T:FSharp.Either.Either`2.Right"></see> value is converted into an array with one element, while <see cref="T:FSharp.Either.Either`2.Left"></see> value is converted into an empty array</para> </summary>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns>An array with one element if <paramref name="either"></paramref> is <see cref="T:FSharp.Either.Either`2.Right"></see>, empty array otherwise.</returns>
    val toArray : Either<'a, 'b> -> 'b []

    /// <summary> Converts <paramref name="either"></paramref> into an <see cref="T:Microsoft.FSharp.Core.Option`1"></see>. 
    /// <para> <see cref="T:FSharp.Either.Either`2.Right"></see> value is converted into <see cref="T:Microsoft.FSharp.Core.Option`1.Some">, while <see cref="T:FSharp.Either.Either`2.Left"></see> value is converted into <see cref="T:Microsoft.FSharp.Core.Option`1.None"></see> </para> </summary>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns><see cref="T:Microsoft.FSharp.Core.Option`1.Some"> if <paramref name="either"></paramref> is <see cref="T:FSharp.Either.Either`2.Right"></see>, <see cref="T:Microsoft.FSharp.Core.Option`1.None"></see> otherwise.</returns>
    val toOption : Either<'a, 'b> -> 'b option

    /// <summary> Returns the wrapped value if <paramref name="either"></paramref> is <see cref="T:FSharp.Either.Either`2.Right"></see>, or given default otherwise. </summary>
    /// <param name="defaultVal">Default value</param>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns>Wrapped value if <paramref name="either"></paramref> is <see cref="T:FSharp.Either.Either`2.Right"></see>, or given default otherwise.</returns>
    val getOrElse : (unit -> 'b) -> Either<'a, 'b> -> 'b

    /// <summary> Returns <see cref="T:FSharp.Either.Either`2.Right"></see> if <paramref name="either"></paramref> is <see cref="T:FSharp.Either.Either`2.Right"></see>, otherwise returns specified value</summary>
    /// <param name="defaultEither">Default either</param>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <returns><paramref name="either"></paramref> if it is <see cref="T:FSharp.Either.Either`2.Right"></see>, otherwise returns specified value</returns>
    val orElse : (unit -> Either<'c,'b>) -> Either<'a, 'b> -> Either<'c, 'b>

[<AutoOpen>]
module Operators =
    /// <summary> Operator alias for <see cref="Either.bind"></see></summary>
    /// <param name="either"><see cref="T:FSharp.Either.Either`2"></see> instance</param>
    /// <param name="binder">Binding function</param>
    /// <returns> New <see cref="T:FSharp.Either.Either`2"></see> instance with <see cref="T:FSharp.Either.Either`2.Right"></see> value bound</returns>
    val inline (>>=) : Either<'a, 'b> -> ('b -> Either<'a, 'c>) -> Either<'a,'c>
    
   